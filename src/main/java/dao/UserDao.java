package dao;

import java.util.ArrayList;

import model.User;

public interface UserDao {
	ArrayList<User> allUsers();
	User createUser(User user);
	void deleteUser(User user);
	User findUserByEmail(String email);
	User findUserById(int id); 
	User updateUser(User user);
}
