<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*"  import="java.io.*" %>
<!DOCTYPE>
<html>
<head>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/main.css">
<link rel="shortcut icon" href="${pageContext.request.contextPath}/favicon.ico" type="image/x-icon" >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Google Chew::Login</title>
</head>
<body>
		
	<div class="top">			
		<center><div class="top-nav">
			<div class="logo">Google Chew</div>
						
		</div></center>
	</div>
	
	<div class="container">
		<div class="header">
			<h1>Login </h1>
		</div>
		
		
		<div class="body">
			<center><form action="logged" method="post">
				<table id="login_table">					
						<tr>
							<td valign="top"><input  type="text" name="email" maxlength="50" size="30" placeholder="email"></td>
						</tr>
						<tr>
	 						<td valign="top"><input  type="password" name="password" maxlength="50" size="30" placeholder="password"></td>
						</tr>
						
						<tr><td>
							<center> <input type="submit" value="Sign In">  </center> 
						</td></tr>					
				</table>
			</form></center>	
		</div><!-- end .body -->
		
		
		<footer>
			<p id="text">footer</p>
		</footer><!-- end .footer -->
		
	</div><!-- end .container -->
	
</body>
</html>