package model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Exhibit")
public class Exhibit {
	public Collection<Exhibition> getExhibitions() {
		return exhibitions;
	}
	
	public void setExhibitions(Collection<Exhibition> exhibitions) {
		this.exhibitions = exhibitions;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (author == null ? 0 : author.hashCode());
		result = prime * result + (condition == null ? 0 : condition.hashCode());
		result = prime * result + (dateMade == null ? 0 : dateMade.hashCode());
		result = prime * result + (description == null ? 0 : description.hashCode());
		result = prime * result + (evaluationValue == null ? 0 : evaluationValue.hashCode());
		result = prime * result + (exhibitionGroup == null ? 0 : exhibitionGroup.hashCode());
		result = prime * result + (exhibitions == null ? 0 : exhibitions.hashCode());
		result = prime * result + id;
		result = prime * result + (inventoryNumber == null ? 0 : inventoryNumber.hashCode());
		result = prime * result + (isOnExhibition ? 1231 : 1237);
		result = prime * result + (isOnSpecialAcc ? 1231 : 1237);
		result = prime * result + (material == null ? 0 : material.hashCode());
		result = prime * result + (name == null ? 0 : name.hashCode());
		result = prime * result + (origin == null ? 0 : origin.hashCode());
		result = prime * result + (technique == null ? 0 : technique.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Exhibit other = (Exhibit) obj;
		if (author == null) {
			if (other.author != null) {
				return false;
			}
		}
		else if (!author.equals(other.author)) {
			return false;
		}
		if (condition == null) {
			if (other.condition != null) {
				return false;
			}
		}
		else if (!condition.equals(other.condition)) {
			return false;
		}
		if (dateMade == null) {
			if (other.dateMade != null) {
				return false;
			}
		}
		else if (!dateMade.equals(other.dateMade)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		}
		else if (!description.equals(other.description)) {
			return false;
		}
		if (evaluationValue == null) {
			if (other.evaluationValue != null) {
				return false;
			}
		}
		else if (!evaluationValue.equals(other.evaluationValue)) {
			return false;
		}
		if (exhibitionGroup == null) {
			if (other.exhibitionGroup != null) {
				return false;
			}
		}
		else if (!exhibitionGroup.equals(other.exhibitionGroup)) {
			return false;
		}
		if (exhibitions == null) {
			if (other.exhibitions != null) {
				return false;
			}
		}
		else if (!exhibitions.equals(other.exhibitions)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (inventoryNumber == null) {
			if (other.inventoryNumber != null) {
				return false;
			}
		}
		else if (!inventoryNumber.equals(other.inventoryNumber)) {
			return false;
		}
		if (isOnExhibition != other.isOnExhibition) {
			return false;
		}
		if (isOnSpecialAcc != other.isOnSpecialAcc) {
			return false;
		}
		if (material == null) {
			if (other.material != null) {
				return false;
			}
		}
		else if (!material.equals(other.material)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		}
		else if (!name.equals(other.name)) {
			return false;
		}
		if (origin == null) {
			if (other.origin != null) {
				return false;
			}
		}
		else if (!origin.equals(other.origin)) {
			return false;
		}
		if (technique == null) {
			if (other.technique != null) {
				return false;
			}
		}
		else if (!technique.equals(other.technique)) {
			return false;
		}
		return true;
	}
	
	@ManyToMany(mappedBy = "exhibits", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Collection<Exhibition>	exhibitions;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "exhibitID")
	private int						id;
	
	@Column(name = "inventID")
	private String					inventoryNumber;
	
	@Column(name = "name")
	private String					name;
	
	@Column(name = "material")
	private String					material;
	
	@Column(name = "technique")
	private String					technique;
	
	@Column(name = "dateMade")
	private String					dateMade;
	
	@Column(name = "origin")
	private String					origin;
	
	@Column(name = "author")
	private String					author;
	
	@Column(name = "condit")
	private String					condition;
	
	@Column(name = "eval")
	private String					evaluationValue;
	
	@Column(name = "descr")
	private String					description;
	
	@Column(name = "onExh")
	private boolean					isOnExhibition;
	
	@Column(name = "exhGroup")
	public String					exhibitionGroup;
	
	@Column(name = "specRec")
	private boolean					isOnSpecialAcc;
	
	public Exhibit() {
		super();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getInventoryNumber() {
		return inventoryNumber;
	}
	
	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMaterial() {
		return material;
	}
	
	public void setMaterial(String material) {
		this.material = material;
	}
	
	public String getTechnique() {
		return technique;
	}
	
	public void setTechnique(String technique) {
		this.technique = technique;
	}
	
	public String getDateMade() {
		return dateMade;
	}
	
	public void setDateMade(String dateMade) {
		this.dateMade = dateMade;
	}
	
	public String getOrigin() {
		return origin;
	}
	
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getCondition() {
		return condition;
	}
	
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	public String getEvaluationValue() {
		return evaluationValue;
	}
	
	public void setEvaluationValue(String evaluationValue) {
		this.evaluationValue = evaluationValue;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isOnExhibition() {
		return isOnExhibition;
	}
	
	public void setOnExhibition(boolean isOnExhibition) {
		this.isOnExhibition = isOnExhibition;
	}
	
	public String getExhibitionGroup() {
		return exhibitionGroup;
	}
	
	public void setExhibitionGroup(String exhibitionGroup) {
		this.exhibitionGroup = exhibitionGroup;
	}
	
	public boolean isOnSpecialAcc() {
		return isOnSpecialAcc;
	}
	
	public void setOnSpecialAcc(boolean isOnSpecialAcc) {
		this.isOnSpecialAcc = isOnSpecialAcc;
	}
	
	public Exhibit(int id, String inventoryNumber, String name, String material, String technique, String dateMade, String origin,
			String author, String condition, String evaluationValue, String description, boolean isOnExhibition, String exhibitionGroup,
			boolean isOnSpecialAcc) {
		super();
		this.id = id;
		this.inventoryNumber = inventoryNumber;
		this.name = name;
		this.material = material;
		this.technique = technique;
		this.dateMade = dateMade;
		this.origin = origin;
		this.author = author;
		this.condition = condition;
		this.evaluationValue = evaluationValue;
		this.description = description;
		this.isOnExhibition = isOnExhibition;
		this.exhibitionGroup = exhibitionGroup;
		this.isOnSpecialAcc = isOnSpecialAcc;
	}
	
	public Exhibit(String inventoryNumber, String name, String material, String technique, String dateMade, String origin,
			String author, String condition, String evaluationValue, String description, String exhibitionGroup) {
		super();
		this.inventoryNumber = inventoryNumber;
		this.name = name;
		this.material = material;
		this.technique = technique;
		this.dateMade = dateMade;
		this.origin = origin;
		this.author = author;
		this.condition = condition;
		this.evaluationValue = evaluationValue;
		this.description = description;
		this.exhibitionGroup = exhibitionGroup;
	}
	
	@Override
	public String toString() {
		return "Exhibit [id=" + id + ", inventoryNumber=" + inventoryNumber + ", name=" + name + ", material=" + material + ", technique="
				+ technique + ", dateMade=" + dateMade + ", origin=" + origin + ", author=" + author + ", condition=" + condition
				+ ", evaluationValue=" + evaluationValue + ", description=" + description + ", isOnExhibition=" + isOnExhibition
				+ ", exhibitionGroup=" + exhibitionGroup + ", isOnSpecialAcc=" + isOnSpecialAcc + "]";
	}
}
