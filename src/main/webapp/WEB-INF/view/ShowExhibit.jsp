<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*"  import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Exhibits</title>
<%@include file = "SideBar.jsp" %>
<%User u = (User)session.getAttribute("loggedUser");%>
				<form action="group" method="GET">
					<input type="hidden" name = "group" value="${exhibit.exhibitionGroup}">
					<a href="javascript:;\" onclick="parentNode.submit();"><h2>${exhibit.exhibitionGroup}</h2></a>
				</form>
				
				<!-- ${is_on_exh}-->
				<form action="edit_item" method="post">
				<input type="hidden" name = "id" value="${exhibit.id}">
				<table>
					<tr>
						<td valign="top" colspan="2"><h3><span>${exhibit.inventoryNumber} ${exhibit.name}</span></h3></td>
					</tr>
					<tr><td  colspan="2" style="text-align: center;"><img alt="${exhibit.inventoryNumber}" height="300px"
					 src="${pageContext.request.contextPath}/styles/img/${exhibit.inventoryNumber}.png"></td></tr>
					<tr>
						<td valign="top"><label for="material"><i>Матеріал</i></label></td>
						<td valign="top"><span>${exhibit.material}</span></td>
					</tr>
					<tr>
						<td valign="top"><label for="technique"><i>Техніка</i></label></td>
						<td valign="top"><span>${exhibit.technique}</span></td>
					</tr>
					<tr>
						<td valign="top"><label for="dateMade"><i>Датування</i></label></td>
						<td valign="top"><span>${exhibit.dateMade}</span></td>
					</tr>
					<tr>
						<td valign="top"><label for="origin"><i>Місце виготовлення</i></label></td>
						<td valign="top"><span>${exhibit.origin}</span></td>
					</tr>
					<tr>
						<td valign="top"><label for="author"><i>Автор</i></label></td>
 						<td valign="top">${exhibit.author}</td>
					</tr>
					<tr>
						<td valign="top"><label for="condition"><i>Стан</i></label></td>
						<td valign="top"><span>${exhibit.condition}</span></td>
					</tr>					
					<tr>
						<td valign="top"><label for="description"><i>Нотатки</i></label></td>
						<td valign="top">${exhibit.description}</td>
					</tr>
					<tr>
						<td><input type="submit" value="Редагувати">  </form> </td>
						<% if (u.isAdmin()==true)  { %>
						<%@include file = "snippets/RemoveItem.jsp" %>
						<%} %>
					</tr>				
			</table>
			
<%@include file = "BottomThings.jsp" %>		