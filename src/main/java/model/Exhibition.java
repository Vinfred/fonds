package model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Exhibition")
public class Exhibition {
	
	@Override
	public String toString() {
		return "Exhibition [exhibits=" + exhibits + ", id=" + id + ", name=" + name + ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + ", city=" + city
				+ ", country=" + country + ", organization=" + organization + ", active=" + active + "]";
	}
	
	public Collection<Exhibit> getExhibits() {
		return exhibits;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active == null ? 0 : active.hashCode());
		result = prime * result + (city == null ? 0 : city.hashCode());
		result = prime * result + (country == null ? 0 : country.hashCode());
		result = prime * result + (dateEnd == null ? 0 : dateEnd.hashCode());
		result = prime * result + dateStart;
		result = prime * result + (exhibits == null ? 0 : exhibits.hashCode());
		result = prime * result + id;
		result = prime * result + (name == null ? 0 : name.hashCode());
		result = prime * result + (organization == null ? 0 : organization.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Exhibition other = (Exhibition) obj;
		if (active == null) {
			if (other.active != null) {
				return false;
			}
		}
		else if (!active.equals(other.active)) {
			return false;
		}
		if (city == null) {
			if (other.city != null) {
				return false;
			}
		}
		else if (!city.equals(other.city)) {
			return false;
		}
		if (country == null) {
			if (other.country != null) {
				return false;
			}
		}
		else if (!country.equals(other.country)) {
			return false;
		}
		if (dateEnd == null) {
			if (other.dateEnd != null) {
				return false;
			}
		}
		else if (!dateEnd.equals(other.dateEnd)) {
			return false;
		}
		if (dateStart != other.dateStart) {
			return false;
		}
		if (exhibits == null) {
			if (other.exhibits != null) {
				return false;
			}
		}
		else if (!exhibits.equals(other.exhibits)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		}
		else if (!name.equals(other.name)) {
			return false;
		}
		if (organization == null) {
			if (other.organization != null) {
				return false;
			}
		}
		else if (!organization.equals(other.organization)) {
			return false;
		}
		return true;
	}
	
	public void setExhibits(Collection<Exhibit> exhibits) {
		this.exhibits = exhibits;
	}
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "GoToExh",
			joinColumns = { @JoinColumn(name = "exhibitionID", referencedColumnName = "exhibitionID") },
			inverseJoinColumns = { @JoinColumn(name = "exhibitID", referencedColumnName = "exhibitID") })
	private Collection<Exhibit>	exhibits;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "exhibitionID")
	private int					id;
	private String				name;
	private int					dateStart;
	private Integer				dateEnd	= 0;
	private String				city;
	private String				country;
	private String				organization;
	@Column(name = "isActive")
	private Boolean				active;
	
	public Exhibition(Collection<Exhibit> exhibits, int id, String name, int dateStart, Integer dateEnd, String city, String country, String organization,
			Boolean active) {
		super();
		this.exhibits = exhibits;
		this.id = id;
		this.name = name;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.city = city;
		this.country = country;
		this.organization = organization;
		this.active = active;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public void setDateEnd(Integer dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	public Exhibition(int id, String name, int dateStart, int dateEnd, String city, String country, String organization) {
		super();
		this.id = id;
		this.name = name;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.city = city;
		this.country = country;
		this.organization = organization;
	}
	
	public Exhibition(int id, String name, int dateStart, String city, String country, String organization) {
		super();
		this.id = id;
		this.name = name;
		this.dateStart = dateStart;
		this.city = city;
		this.country = country;
		this.organization = organization;
	}
	
	public Exhibition() {
		super();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getDateStart() {
		return dateStart;
	}
	
	public void setDateStart(int dateStart) {
		this.dateStart = dateStart;
	}
	
	public int getDateEnd() {
		return dateEnd;
	}
	
	public void setDateEnd(int dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getOrganization() {
		return organization;
	}
	
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
}
