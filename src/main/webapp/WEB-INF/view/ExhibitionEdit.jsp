<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*"  import="help.*"  import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Edit Exhibition</title>
<%@include file = "SideBar.jsp" %>
				<form action="save" method="POST">
				<input type="hidden" name = "id" value="${exhibition.id}">
				<table>
					<tr>
						<td valign="top"><label for="name"><i>Назва</i></label></td>
						<td><input  type="text" name="name" maxlength="50" size="50" value="${exhibition.name}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="dateStart"><i>Дата початку</i></label></td>
						<td><input  type="text" name="dateStart" maxlength="50" size="50" value="${exhibition.dateStart}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="dateEnd"><i>Дата закінчення</i></label></td><c:set value="${exhibition.dateEnd}" var="date"/>
						<td><input  type="text" name="dateEnd" maxlength="50" size="50" 
						value="<c:out value="${date eq 0 ? '': date}"/>"></td>
					</tr>
					<tr>
						<td valign="top"><label for="organization"><i>Організація</i></label></td>
						<td valign="top"><input  type="text" name="organization" maxlength="50" size="50" value="${exhibition.organization}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="city"><i>Місто</i></label></td>
						<td valign="top"><input  type="text" name="city" maxlength="50" size="50" value="${exhibition.city}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="country"><i>Країна</i></label></td>
						<td valign="top"><input  type="text" name="country" maxlength="50" size="50" value="${exhibition.country}"></td>
					</tr>
					<tr><td>
						 <input type="submit" value="Зберегти">  
					</td></tr>				
			</table></form> 

<%@include file = "BottomThings.jsp" %>	