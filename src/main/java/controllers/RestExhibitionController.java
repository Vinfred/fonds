package controllers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Exhibition;
import model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import dao.ExhibitionDao;

@Controller
@RequestMapping(value = "/exhibitions")
public class RestExhibitionController {
	@Autowired
	private HttpServletRequest	context;
	@Autowired
	private ExhibitionDao		exhibitionDAO;
	
	@RequestMapping(method = RequestMethod.GET, value = "/edit")
	String edit() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return context.getContextPath() + "\\=\\login.jsp";
		}
		else {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			Exhibition uu = exhibitionDAO.findExhibitionById(id);
			context.setAttribute("exhibition", uu);
			
			return "ExhibitionEdit.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/show")
	String showExhibition() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return context.getContextPath() + "\\=\\login.jsp";
		}
		else {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			Exhibition e = exhibitionDAO.findExhibitionById(id);
			System.out.println(e);
			
			context.setAttribute("exhibition", e);
			context.setAttribute("exhibits", e.getExhibits());
			
			return "ShowExhibition.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save")
	String save() {
		HttpSession session = context.getSession();
		User user = (User) session.getAttribute("loggedUser");
		
		if (user == null) {
			return context.getContextPath() + "\\=\\login.jsp";
		}
		else {
			try {
				context.setCharacterEncoding("UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			Exhibition e = exhibitionDAO.findExhibitionById(Integer.valueOf(context.getParameter("id")));
			String name = context.getParameter("name");
			Integer dateStart = Integer.valueOf(context.getParameter("dateStart"));
			Integer dateEnd = context.getParameter("dateEnd") == "" ? 0 : Integer.valueOf(context.getParameter("dateEnd"));
			String organization = context.getParameter("organization");
			String city = context.getParameter("city");
			String country = context.getParameter("country");
			
			Exhibition ee = new Exhibition(e.getId(), name, dateStart, dateEnd, city, country, organization);
			exhibitionDAO.updateExhibition(ee);
			
			return showExhibition();
			
		}
	}
	
	@RequestMapping(method = RequestMethod.GET)
	String exhibitions() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			System.out.println(context.getContextPath());
			return context.getContextPath() + "\\=\\login.jsp";
		}
		else {
			ArrayList<Exhibition> e = exhibitionDAO.getAllExhibitions();
			System.out.println(e.size());
			context.setAttribute("exhibitions", e);
			
			return "ExhibitionsList.jsp";
		}
	}
	
}
