package dao;

import help.Constants;

import java.sql.Connection;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;

import model.Exhibit;
import model.Exhibition;
import model.User;

import org.springframework.transaction.annotation.Transactional;

public class DaoImpl implements UserDao, ExhibitDao, ExhibitionDao, Constants {
	@PersistenceContext
	private EntityManager	em;
	
	private CriteriaBuilder	criteriaBuilder;
	
	Connection				connection	= null;
	
	public DaoImpl(Connection connection) {
		this.connection = connection;
	}
	
	public DaoImpl() {
	}
	
	@Override
	public ArrayList<User> allUsers() {
		ArrayList<User> users = new ArrayList<User>(em.createQuery("SELECT u FROM User u").getResultList());
		return users;
	}
	
	@Override
	@Transactional
	public User createUser(User user) {
		em.persist(user);
		return user;
	}
	
	@Override
	@Transactional
	public void deleteUser(User user) {
		em.remove(em.contains(findUserById(user.getId())) ? findUserById(user.getId()) : em.merge(findUserById(user.getId())));
		// this.em.remove(findUserById(user.getId()));
	}
	
	@Override
	public User findUserByEmail(String email) {
		System.out.println("find user by email: " + email);
		try {
			return (User) em.createQuery("SELECT u FROM User u WHERE u.email LIKE :email").setParameter("email", email).getSingleResult();
		}
		catch (Exception ex) {
			return null;
		}
	}
	
	@Override
	public User findUserById(int id) {
		System.out.println("find user by id: " + id);
		User u = em.find(User.class, id);
		return u;
	}
	
	@Override
	@Transactional
	public User updateUser(User user) {
		System.out.println("update user: " + user.getEmail());
		return em.merge(user);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Exhibit> getAllExhibits() {
		ArrayList<Exhibit> exhibits = new ArrayList<>(em.createQuery("SELECT u FROM Exhibit u").getResultList());
		return exhibits;
	}
	
	@Override
	@Transactional
	public Exhibit createExhibit(Exhibit exhibit) {
		em.persist(exhibit);
		return exhibit;
	}
	
	@Override
	@Transactional
	public void deleteExhibit(Exhibit exhibit) {
		em.remove(em.contains(findExhibitById(exhibit.getId())) ? findExhibitById(exhibit.getId()) :
				em.merge(findExhibitById(exhibit.getId())));
		// em.remove(findExhibitById(exhibit.getId()));
	}
	
	@Override
	public Exhibit findExhibitById(int id) {
		System.out.println("find exhibit by id: " + id);
		Exhibit u = em.find(Exhibit.class, id);
		return u;
	}
	
	@Override
	@Transactional
	public Exhibit updateExhibit(Exhibit exhibit) {
		System.out.println("update exhibit: " + exhibit.getName());
		return em.merge(exhibit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Exhibit> getSpecificGroup(String group) {
		/* criteriaBuilder = em.getCriteriaBuilder(); CriteriaQuery<Exhibit> cq = criteriaBuilder.createQuery(Exhibit.class); Root<Exhibit> ex =
		 * cq.from(Exhibit.class);
		 * 
		 * Predicate predicate1 = criteriaBuilder.like(ex.<String>get("exhibitionGroup"), group);
		 * 
		 * 
		 * //Predicate condition = qb.gt(ex.get(Exhibit_.exhibitionGroup), group); cq.where(predicate1); TypedQuery<Exhibit> q =
		 * em.createQuery(cq); ArrayList<Exhibit> result = new ArrayList<> (q.getResultList()); */
		
		ArrayList<Exhibit> exhibits = new ArrayList<>(em.createQuery("SELECT u FROM Exhibit u WHERE u.exhibitionGroup LIKE :group")
				.setParameter("group", group).getResultList());
		return exhibits;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Exhibit> findExhibitByName(String name) {
		ArrayList<Exhibit> e = new ArrayList<>(em.createQuery("SELECT u FROM Exhibit u WHERE LOWER (u.name) LIKE :name").setParameter("name", "%" + name + "%")
				.getResultList());
		
		if (e.size() == 0) {
			e = new ArrayList<>(em.createQuery("SELECT u FROM Exhibit u WHERE LOWER (u.inventoryNumber) LIKE :name").setParameter("name", "%" + name + "%")
					.getResultList());
		}
		return e;
	}
	
	// i didn't manage to turn JPA2 on :_( :_(
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Exhibit> search(Exhibit e, String id, String isOnExhibition, String isOnSpecialAcc) {
		System.out.println(e);
		String query = "SELECT u FROM Exhibit u WHERE " +
				"LOWER (u.id) LIKE '%' AND " +
				"LOWER (u.inventoryNumber) LIKE " + "'%" + e.getInventoryNumber() + "%'" + " AND " +
				"LOWER (u.name) LIKE " + "'%" + e.getName() + "%'" + " AND " +
				"LOWER (u.material) LIKE " + "'%" + e.getMaterial() + "%'" + " AND " +
				"LOWER (u.technique) LIKE " + "'%" + e.getTechnique() + "%'" + " AND " +
				"LOWER (u.dateMade) LIKE " + "'%" + e.getDateMade() + "%'" + " AND " +
				"LOWER (u.origin) LIKE " + "'%" + e.getOrigin() + "%'" + " AND " +
				"LOWER (u.author) LIKE " + "'%" + e.getAuthor() + "%'" + " AND " +
				"LOWER (u.condition) LIKE '%' AND " +
				"LOWER (u.evaluationValue) LIKE " + "'%" + e.getEvaluationValue() + "%'" + " AND " +
				"LOWER (u.description) LIKE '%' AND " +
				"LOWER (u.isOnExhibition) LIKE " + "'%" + isOnExhibition + "%'" + " AND " +
				"LOWER (u.isOnSpecialAcc) LIKE  " + "'%" + isOnSpecialAcc + "%'";
		System.out.println("Query: " + query);
		ArrayList<Exhibit> exhibits = new ArrayList<>(em.createQuery(query).getResultList());
		
		return exhibits;
	}
	
	@Override
	public ArrayList<Exhibition> getAllExhibitions() {
		@SuppressWarnings("unchecked")
		ArrayList<Exhibition> exhibitions = new ArrayList<>(em.createQuery("SELECT u FROM Exhibition u").getResultList());
		return exhibitions;
	}
	
	@Override
	public ArrayList<Exhibition> search(Exhibition exhibition) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional
	public Exhibition createExhibition(Exhibition exhibition) {
		em.persist(exhibition);
		return exhibition;
	}
	
	@Override
	@Transactional
	public void deleteExhibition(Exhibition exhibition) {
		em.remove(findExhibitionById(exhibition.getId()));
	}
	
	@Override
	public Exhibition findExhibitionById(int id) {
		System.out.println("find exhibition by id: " + id);
		Exhibition u = em.find(Exhibition.class, id);
		return u;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Exhibition> findExhibitionByName(String name) {
		ArrayList<Exhibition> e = new ArrayList<>(em.createQuery("SELECT u FROM Exhibitiom u WHERE LOWER (u.name) LIKE :name")
				.setParameter("name", "%" + name + "%")
				.getResultList());
		
		return e;
	}
	
	@Override
	@Transactional
	public Exhibition updateExhibition(Exhibition exhibition) {
		System.out.println("update exhibition: " + exhibition.getName());
		return em.merge(exhibition);
	}
}
