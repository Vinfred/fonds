<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*" import="help.*" import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Google Chew::Profile</title>
<%@include file = "SideBar.jsp" %>
				<%User u = (User)session.getAttribute("loggedUser");%>
				<form action="editUser" method="post">
				<input type="hidden" name = "id" value="<%out.print(u.getId()); %>">
				<input type="hidden" name = "origin" value="<%out.print(Constants.MY_PAGE); %>">
				<table id="profile">
					<tr>
						<td rowspan="5" width="225px"><img alt="<%out.print(u.getEmail()); %>" width="200px"
					 	src="${pageContext.request.contextPath}/styles/img/<%out.print(u.getEmail()); %>.jpg"></td>
					 	<td>
					 		<table>
					 			<tr>
									<td valign="top">
										<span><b><%out.print(u.getName()==null?"":u.getName()); %>
										<%out.print(u.getSurname()==null?"":u.getSurname()); %></b></span>
									</td>
								</tr>
								<tr>
									<td valign="top"><%out.print(u.getPosition()==null?"":u.getPosition()); %></td>
								</tr>
					 		</table>					 		
					 	</td>
					 </tr>													
			</table>
			<br/><input type="submit" value="Редагувати інформацію">  
			</form>
<%@include file = "BottomThings.jsp" %>	