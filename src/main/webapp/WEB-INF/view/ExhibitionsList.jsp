<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*" import="help.*" import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Exhibitions</title>
<%@include file = "SideBar.jsp" %>
	<table id="exhibits">
		<c:forEach var="exhibition" items="${exhibitions}">
			<tr>
				<td>${exhibition.id}</td>
				
				<td>
					<form action="exhibitions/show" method="GET">
					<input type="hidden" name = "id" value="${exhibition.id}">
					<a href="javascript:;\" onclick="parentNode.submit();">
						<b><i>${exhibition.name} (${exhibition.country}, ${exhibition.dateStart} - 
							<c:set value="${exhibition.dateEnd}" var="date"/><c:out value="${date eq 0 ? '': date}"/>)
						</i></b>
						<span style="color:red"><c:out value="${date eq 0 ? 'активна': ''}"/></span>
					</a>
					</form>
				</td>							
			</tr>
		</c:forEach>
	</table>	
<%@include file = "BottomThings.jsp" %>	