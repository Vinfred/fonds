package dao;

import java.util.ArrayList;

import model.Exhibition;

public interface ExhibitionDao {
	ArrayList<Exhibition> getAllExhibitions();
	
	ArrayList<Exhibition> search(Exhibition exhibition);
	
	Exhibition createExhibition(Exhibition exhibition);
	
	void deleteExhibition(Exhibition exhibition);
	
	Exhibition findExhibitionById(int id);
	
	ArrayList<Exhibition> findExhibitionByName(String name);
	
	Exhibition updateExhibition(Exhibition exhibition);
	
}
