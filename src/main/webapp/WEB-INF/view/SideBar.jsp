<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    </head>
	<body>
    <div class="top">			
		<div class="top-nav">
		<section>
			<div id="login"><table id="login-table">
					<tr><td>
						<a href="${pageContext.request.contextPath}\=\profile" class="orange">${loggedUser.email}</a>
					</td></tr>
					<tr><td>
						<a href="${pageContext.request.contextPath}\=\logOut">Log Out</a>
					</td></tr>				
			</table></div>
			<div class="search">
			<%@include file = "Search.jsp" %>
			</div>
		</section>			
		</div>
	</div>
	<div class="container">
	
		<div class="body">
			<div id="sidebar">
				<ul>
					<li><a href="${pageContext.request.contextPath}\=\profile">Моя сторінка </a></li>
					<li><a href="${pageContext.request.contextPath}\=\exhibits">Експонати</a></li>
					<li><a href="${pageContext.request.contextPath}\=\exhibitions">Виставки </a></li>
					<li><a href="${pageContext.request.contextPath}\=\search">Пошук </a></li>
					<li><a href="#">Налаштування </a></li>
					<li><a href="${pageContext.request.contextPath}\=\users">Manage Users</a></li>
				</ul>
			</div>
			
			<div class="main">