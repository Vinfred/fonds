package dao;

import java.util.ArrayList;

import model.Exhibit;

public interface ExhibitDao {
	ArrayList<Exhibit> getAllExhibits();
	
	ArrayList<Exhibit> search(Exhibit exhibit, String id, String isOnExhibition, String isOnSpecialAcc);
	
	ArrayList<Exhibit> getSpecificGroup(String group);
	
	Exhibit createExhibit(Exhibit exhibit);
	
	void deleteExhibit(Exhibit exhibit);
	
	Exhibit findExhibitById(int id);
	
	ArrayList<Exhibit> findExhibitByName(String name);
	
	Exhibit updateExhibit(Exhibit exhibit);
	
}
