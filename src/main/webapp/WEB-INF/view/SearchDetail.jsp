<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Search</title>
<%@include file = "SideBar.jsp" %>

	<form action="search_all" method="post">
				<input type="hidden" name = "id" value="${exhibit.id}">
				<table>
					<tr><td><br/></td></tr>
					<tr>
						<td valign="top"><label for="inventoryNumber"><i>Інвентарний №</i></label></td>
						<td valign="top"><input  type="text" name="inventoryNumber" maxlength="50" size="50" placeholder="інвентарний номер" value="${exhibit.inventoryNumber}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Назва</i></label></td>
						<td valign="top"><input  type="text" name="name" maxlength="50" size="50" placeholder="назва" value="${exhibit.name}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Матеріал</i></label></td>
						<td valign="top"><textarea name="material" rows="5" cols="50" placeholder="material">${exhibit.material}</textarea></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Техніка</i></label></td>
						<td valign="top"><input  type="text" name="technique" maxlength="50" size="50" placeholder="техніка" value="${exhibit.technique}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Датування</i></label></td>
						<td valign="top"><input  type="text" name="dateMade" maxlength="50" size="50" value="${exhibit.dateMade}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Місце виготовлення</i></label></td>
						<td valign="top"><input  type="text" name="origin" maxlength="50" size="50" value="${exhibit.origin}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="surname"><i>Автор</i></label></td>
						<td valign="top"><input  type="text" name="author" maxlength="50" size="50" value="${exhibit.author}"></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="Шукати!"></form>
						</td>
					</tr>				
			</table>

<%@include file = "BottomThings.jsp" %>	