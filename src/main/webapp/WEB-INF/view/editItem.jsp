<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*"  import="help.*" import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Google Chew::Edit Exhibit</title>
<%@include file = "SideBar.jsp" %>
				<form action="group" method="GET">
					<input type="hidden" name = "group" value="${exhibit.exhibitionGroup}">
					<a href="javascript:;\" onclick="parentNode.submit();"><h2>${exhibit.exhibitionGroup}</h2></a>
				</form>
				
				<form action="save_item" method="POST">
				<input type="hidden" name = "id" value="${exhibit.id}">
				<table>
					<tr>
						<td valign="top" colspan="2"><h3><span>${exhibit.inventoryNumber} ${exhibit.name}</span></h3></td>
					</tr>
					<tr><td><br/></td></tr>
					<tr>
						<td valign="top"><label for="name"><i>Матеріал</i></label></td>
						<td valign="top"><textarea name="material" rows="5" cols="50" placeholder="material">${exhibit.material}</textarea></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Техніка</i></label></td>
						<td valign="top"><input  type="text" name="technique" maxlength="50" size="50" placeholder="техніка" value="${exhibit.technique}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Датування</i></label></td>
						<td valign="top"><input  type="text" name="dateMade" maxlength="50" size="50" value="${exhibit.dateMade}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Місце виготовлення</i></label></td>
						<td valign="top"><input  type="text" name="origin" maxlength="50" size="50" value="${exhibit.origin}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="surname"><i>Автор</i></label></td>
						<td valign="top"><input  type="text" name="author" maxlength="50" size="50" value="${exhibit.author}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="name"><i>Стан</i></label></td>
						<td valign="top"><textarea name="condition" rows="5" cols="50" placeholder="condition">${exhibit.condition}</textarea></td>
					</tr>					
					<tr>
						<td valign="top"><label for="position"><i>Нотатки</i></label></td>
						<td valign="top"><textarea name="description" rows="5" cols="50" placeholder="description">${exhibit.description}</textarea></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="Зберегти зміни"></form>
							<form action="show_item" method="GET">
								<input type="hidden" name = "id" value="${exhibit.id}">
								<input type="submit" value="Скасувати">
							</form>
						</td>
					</tr>				
			</table>			
<%@include file = "BottomThings.jsp" %>	