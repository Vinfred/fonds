package model;

import help.Md5;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {
	@Column(name="position")
	private String position;

	@Column(name="admin")
	private Boolean admin;

	public Boolean isAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	@Column(name="email")
	private String email;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;

	@Column(name="name")
	private String name;	

	@Column(name="password")
	private String password;

	@Column(name="surname")
	private String surname;

	public User() {
		super();
	}

	public User(int id, String email, String password) {
		super();
		this.id = id;
		this.email = email;
		this.password =  password;
		System.out.println("booboo");
	}

	public User(int id, String email, String password, String position,
			String name, String surname, Boolean admin) {
		super();
		this.id = id;
		this.position = position;
		this.email = email;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.admin = admin;
	}


	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public User(String email, String password) {
		super();
		this.email = email;
		this.password = Md5.getMd5(password);
	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getSurname() {
		return surname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", password=" + password
				+ ", position=" + position +", name=" + name + ", surname=" + surname + "]";
	}	
}
