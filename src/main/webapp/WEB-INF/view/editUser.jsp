<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*" import="help.*"  import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Google Chew::Edit Profile</title>
<%@include file = "SideBar.jsp" %>
		<%User u = (User)session.getAttribute("loggedUser");%>
			<form action="saveUser" method="POST">
			<input type="hidden" name = "id" value="${user.id}">
			<input type="hidden" name = "origin" value="${origin}">
				<table>
					<tr>
						<td valign="top"><label for="name">Name</label></td>
						<td valign="top"><input  type="text" name="name" maxlength="50" size="30" value="${user.name}"></td>
					</tr>
					<tr>
						<td valign="top"><label for="surname">Surname</label></td>
 						<td valign="top"><input  type="text" name="surname" maxlength="50" size="30" value="${user.surname}"></td>
					</tr>
				
					<tr>
						<td valign="top"><label for="position">Position</label></td>
						<td valign="top"><input  type="text" name="position" maxlength="50" size="30" value="${user.position}"></td>
					</tr>
					<tr><td>
					<% if (u.isAdmin()==true)  { %>
						<%@include file = "MakeAdmin.jsp" %>
					<%} %>
					</td></tr>				
					<tr><td>
						 <input type="submit" value="Save">   
					</td></tr>				
				</table>
			</form>
<%@include file = "BottomThings.jsp" %>