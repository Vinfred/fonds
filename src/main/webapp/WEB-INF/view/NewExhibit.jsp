<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*" import="help.*" import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Add Exhibit</title>
<%@include file = "SideBar.jsp" %>
	<form action="save_item" method="POST">
				<input type="hidden" name = "id" value="${exhibit.id}">
				<table>
					<tr>
						<td valign="top"><label for="name"><i>Назва</i></label></td>
						<td valign="top"><input  type="text" name="name" maxlength="50" size="50" placeholder="Назва">
					</tr>
					<tr>
						<td valign="top"><label for="inventoryNumber"><i>Інвентарний номер</i></label></td>
						<td valign="top"><input  type="text" name="inventoryNumber" maxlength="50" size="50" placeholder="Інвентарний номер">
					</tr>
					<tr>
						<td valign="top"><label for="exhibitionGroup"><i>Виставкова група</i></label></td>
						<td valign="top"><input  type="text" name="exhibitionGroup" maxlength="50" size="50" placeholder="Виставкова група">
					</tr>
					<tr>
						<td valign="top"><label for="evaluationValue"><i>Оціночна вартість</i></label></td>
						<td valign="top"><input  type="text" name="evaluationValue" maxlength="50" size="50" placeholder="Оціночна вартість">
					</tr>
					<tr><td><br/></td></tr>
					<tr>
						<td valign="top"><label for="material"><i>Матеріал</i></label></td>
						<td valign="top"><textarea name="material" rows="5" cols="50" placeholder="Матеріал"></textarea></td>
					</tr>
					<tr>
						<td valign="top"><label for="technique"><i>Техніка</i></label></td>
						<td valign="top"><input  type="text" name="technique" maxlength="50" size="50" placeholder="Техніка"></td>
					</tr>
					<tr>
						<td valign="top"><label for="dateMade"><i>Датування</i></label></td>
						<td valign="top"><input  type="text" name="dateMade" maxlength="50" size="50" placeholder="Датування"></td>
					</tr>
					<tr>
						<td valign="top"><label for="origin"><i>Місце виготовлення</i></label></td>
						<td valign="top"><input  type="text" name="origin" maxlength="50" size="50" placeholder="Місце виготовлення"></td>
					</tr>
					<tr>
						<td valign="top"><label for="author"><i>Автор</i></label></td>
						<td valign="top"><input  type="text" name="author" maxlength="50" size="50" placeholder="Автор"></td>
					</tr>
					<tr>
						<td valign="top"><label for="condition"><i>Стан</i></label></td>
						<td valign="top"><textarea name="condition" rows="5" cols="50" placeholder="Стан"></textarea></td>
					</tr>					
					<tr>
						<td valign="top"><label for="description"><i>Нотатки</i></label></td>
						<td valign="top"><textarea name="description" rows="5" cols="50" placeholder="Нотатки"></textarea></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="Зберегти"></form>
							<form action="exhibits" method="GET">
								<input type="submit" value="Скасувати">
							</form>
						</td>
					</tr>				
			</table>

<%@include file = "BottomThings.jsp" %>	