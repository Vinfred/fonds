<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*" import="help.*" import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Exhibition</title>
<%@include file = "SideBar.jsp" %>
		<form action="edit" method="GET">
		<c:set value="${exhibition.dateEnd}" var="date"/>
				<input type="hidden" name = "id" value="${exhibition.id}">
				<span style="color:red"><i><c:out value="${date eq 0 ? 'Активна': ''}"/></i></span>
				<table>
					<tr>
						<td><span style=font-weight:bold>
							${exhibition.name} (${exhibition.dateStart} - <c:out value="${date eq 0 ? '': date}"/>)
						</span></td>
					</tr>
					<tr>
						<td valign="top"><span>${exhibition.organization}</span></td>
					</tr>
					<tr>
						<td valign="top"><span><i>${exhibition.city}, ${exhibition.country}</i></span></td>
					</tr>
					<tr><td>
						 <input type="submit" value="Редагувати"> </form>  
						 <form action="add_exhibits" method="GET"><input type="hidden" name = "id" value="${exhibition.id}"><input type="submit" value="Додати експонат"> </form>
					</td></tr>				
			</table>
			
			
			<%@include file = "snippets/Exhibits.jsp" %>	

<%@include file = "BottomThings.jsp" %>	