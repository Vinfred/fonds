package controllers;

import help.Constants;
import help.Md5;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Exhibit;
import model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import dao.ExhibitDao;
import dao.ExhibitionDao;
import dao.UserDao;

@Controller
public class RestController {
	
	@Autowired
	private HttpServletRequest	context;
	@Autowired
	private UserDao				userDAO;
	@Autowired
	private ExhibitDao			exhibitDAO;
	@Autowired
	private ExhibitionDao		exhibitionDAO;
	
	@RequestMapping(method = RequestMethod.GET, value = "/exhibits")
	String exhibits() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			ArrayList<Exhibit> exhibits = exhibitDAO.getAllExhibits();
			System.out.println(exhibits.size());
			context.setAttribute("exhibits", exhibits);
			
			return "showGroup.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/group")
	String specificGroup() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			try {
				context.setCharacterEncoding("UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			String value = context.getParameter("group");
			System.out.println(value);
			
			@SuppressWarnings("unchecked")
			ArrayList<Exhibit> exhibits = value != null ? exhibitDAO.getSpecificGroup(value) : (ArrayList<Exhibit>) context.getAttribute("exhibit");
			
			System.out.println(exhibits.size());
			context.setAttribute("exhibits", exhibits);
			
			String warning = exhibits.size() == 0 ? "Тут нікого немає" : "";
			context.setAttribute("oops", warning);
			
			return "showGroup.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/addUser")
	String addUser() {
		HttpSession session = context.getSession();
		session.getAttribute("loggedUser");
		try {
			context.setCharacterEncoding("UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String email = context.getParameter("email");
		String password = context.getParameter("password");
		
		System.out.println("email: " + email);
		
		if (email != null && password != null && !email.equals("") && !password.equals("")) {
			User u = new User(email, password);
			u.setAdmin(false);
			System.out.println("new user");
			userDAO.createUser(u);
			
			return users();
		}
		else {
			return login();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/deleteUser")
	String deleteUser() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			
			userDAO.deleteUser(userDAO.findUserById(Integer.valueOf(id)));
			
			return users();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/delete_item")
	String deleteItem() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			
			exhibitDAO.deleteExhibit(exhibitDAO.findExhibitById(Integer.valueOf(id)));
			
			return exhibits();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/editUser")
	String editUser() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			User uu = userDAO.findUserById(id);
			context.setAttribute("user", uu);
			
			int origin = Integer.valueOf(context.getParameter("origin"));
			context.setAttribute("origin", origin);
			
			return "editUser.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/edit_item")
	String editExhibit() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			Exhibit e = exhibitDAO.findExhibitById(id);
			context.setAttribute("exhibit", e);
			
			return "editItem.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save_item")
	String saveExhibit() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			Exhibit e = null;
			try {
				context.setCharacterEncoding("UTF-8");
				
			}
			catch (UnsupportedEncodingException ex) {
				ex.printStackTrace();
			}
			
			String name = context.getParameter("material");
			String inventoryNumber = context.getParameter("technique");
			String evaluationValue = context.getParameter("evaluationValue");
			String exhibitionGroup = context.getParameter("exhibitionGroup");
			String material = context.getParameter("material");
			String technique = context.getParameter("technique");
			String dateMade = context.getParameter("dateMade");
			String origin = context.getParameter("origin");
			String author = context.getParameter("author");
			String condition = context.getParameter("condition");
			String description = context.getParameter("description");
			
			try {
				e = exhibitDAO.findExhibitById(Integer.valueOf(context.getParameter("id")));
				Exhibit ee = new Exhibit(e.getId(), e.getInventoryNumber(), e.getName(), material, technique, dateMade, origin, author, condition,
						e.getEvaluationValue(), description, e.isOnExhibition(), e.getExhibitionGroup(), e.isOnSpecialAcc());
				exhibitDAO.updateExhibit(ee);
				return showExh();
			}
			catch (Exception ex) {
				exhibitDAO.createExhibit(new Exhibit(inventoryNumber, name, material, technique, dateMade, origin, author, condition, evaluationValue,
						description, exhibitionGroup));
				return exhibits();
			}
			
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/logged")
	String logged() {
		HttpSession session = context.getSession();
		User u = null;
		
		if (session.getAttribute("loggedUser") != null) {
			return profile();
		}
		else {
			System.out.println("first if");
			String email = context.getParameter("email");
			String password = context.getParameter("password");
			u = userDAO.findUserByEmail(email);
			
			// System.out.println(email);
			// System.out.println(password);
			
			if (u != null) {
				if (Md5.getMd5(password).equals(u.getPassword())) {
					session.setAttribute("loggedUser", u);
					System.out.println(u.getName());
					System.out.println("admin? " + u.isAdmin());
					System.out.println(session.getAttribute("loggedUser"));
					context.setAttribute("user", u);
					
					return exhibits();
				}
				else {
					System.out.println("password fail");
					return login();
				}
			}
			
			else {
				return login();
			}
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/login")
	String login() {
		if (context.getSession().getAttribute("loggedUser") != null) {
			return profile();
		}
		else {
			return "login.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/logOut")
	String logOut() {
		User u = (User) context.getSession().getAttribute("loggedUser");
		if (u == null) {
			return login();
		}
		else {
			context.getSession().removeAttribute("loggedUser");
			return login();
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/profile")
	String profile() {
		User u = (User) context.getSession().getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			return "userMain.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveUser")
	String saveUser() {
		HttpSession session = context.getSession();
		User user = (User) session.getAttribute("loggedUser");
		
		if (user == null) {
			return login();
		}
		else {
			try {
				context.setCharacterEncoding("UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			User lUser = userDAO.findUserById(Integer.valueOf(context.getParameter("id")));
			int origin = Integer.valueOf(context.getParameter("origin"));
			
			String name = context.getParameter("name");
			String surname = context.getParameter("surname");
			String position = context.getParameter("position");
			
			System.out.println(position);
			
			User u = new User(lUser.getId(), lUser.getEmail(), lUser.getPassword(), position, name, surname, lUser.isAdmin());
			userDAO.updateUser(u);
			
			session.setAttribute("loggedUser", u);
			
			if (origin == Constants.MY_PAGE) {
				return profile();
			}
			else {
				return users();
			}
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/show_item")
	String showExh() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		Exhibit e = null;
		
		try {
			String value = context.getParameter("id");
			Integer id = Integer.valueOf(value);
			e = exhibitDAO.findExhibitById(id);
		}
		catch (Exception e2) {
			e = (Exhibit) context.getAttribute("exhibit");
		}
		
		// ArrayList <Message> mesList = groupDao.getAllMessages(g.getId());
		
		if (u == null == Boolean.TRUE) {
			return login();
		}
		else {
			String exhs = e.getExhibitions() != null ? "!!!" : "";
			
			System.out.println(e.getName());
			context.setAttribute("exhibit", e);
			context.setAttribute("is_on_exh", exhs);
			
			return "ShowExhibit.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "exhibitions/add_exhibits")
	String addExhibits() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			return exhibits();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/new_exhibit")
	String newExchibit() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			return "NewExhibit.jsp";
		}
	}
	
	@SuppressWarnings("unused")
	@RequestMapping(method = RequestMethod.GET, value = "/users")
	String users() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		System.out.println(u.getEmail() + "admin? " + u.isAdmin());
		
		if (u == null) {
			return login();
		}
		else {
			if (u.isAdmin()) {
				ArrayList<User> users = userDAO.allUsers();
				
				context.setAttribute("users", users);
				return "jsp.jsp";
			}
			else {
				return "signUp.html";
				// return null;
			}
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/search")
	String search() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			return "SearchDetail.jsp";
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/search_results")
	String searchRes() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			String name = context.getParameter("name");
			System.out.println(name);
			
			return null;
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/search_by_name")
	String searchByName() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		if (u == null) {
			return login();
		}
		else {
			try {
				context.setCharacterEncoding("UTF-8");
			}
			catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			String name = context.getParameter("name");
			System.out.println(name);
			ArrayList<Exhibit> e = exhibitDAO.findExhibitByName(name);
			context.setAttribute("exhibit", e);
			System.out.println("Search result:\n" + e);
			
			if (e.size() == 1) {
				context.setAttribute("exhibit", e.get(0));
				return showExh();
			}
			else {
				context.setAttribute("exhibit", e);
				return specificGroup();
			}
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/search_all")
	String searchAll() {
		HttpSession session = context.getSession();
		User u = (User) session.getAttribute("loggedUser");
		
		try {
			context.setCharacterEncoding("UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		if (u == null) {
			return login();
		}
		else {
			String name = context.getParameter("name") != "" ? context.getParameter("name") : "%";
			String inventoryNumber = context.getParameter("inventoryNumber") != "" ? context.getParameter("inventoryNumber") : "%";
			String material = context.getParameter("material") != "" ? context.getParameter("material") : "%";
			String technique = context.getParameter("technique") != "" ? context.getParameter("technique") : "%";
			String dateMade = context.getParameter("dateMade") != "" ? context.getParameter("dateMade") : "%";
			String origin = context.getParameter("origin") != "" ? context.getParameter("origin") : "%";
			String author = context.getParameter("author") != "" ? context.getParameter("author") : "%";
			
			// String isOnExhibition = context.getParameter("isOnExhibition")!=""?context.getParameter("isOnExhibition"): "%";
			// String isOnSpecialAcc = context.getParameter("isOnSpecialAcc")!=""?context.getParameter("id"): "%";
			
			String condition = "%";
			String evaluationValue = "%";
			String description = "%";
			String id = "%";
			String isOnExhibition = "%";
			String isOnSpecialAcc = "%";
			String exhGroup = "%";
			/* String material = "'%'"; String technique = "'%'"; String dateMade = "'%'"; String origin = "'%'"; String author = "'%'"; String
			 * condition = "'%'"; String evaluationValue = "'%'"; String description = "'%'"; */
			
			Exhibit ex = new Exhibit(inventoryNumber, name, material, technique, dateMade, origin, author, condition, evaluationValue, description, exhGroup);
			System.out.println(ex);
			
			ArrayList<Exhibit> exs = exhibitDAO.search(ex, id, isOnExhibition, isOnSpecialAcc);
			// context.setAttribute("group", exs); */
			
			// ArrayList<Exhibit> e = exhibitDAO.search(e, id, isOnExhibition, isOnSpecialAcc);
			
			System.out.println(name);
			System.out.println("Search result:\n" + exs);
			
			if (exs.size() == 1) {
				context.setAttribute("exhibit", exs.get(0));
				return showExh();
			}
			else {
				context.setAttribute("exhibit", exs);
				return specificGroup();
			}
		}
	}
}
