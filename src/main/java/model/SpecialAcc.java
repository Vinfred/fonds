package model;

public class SpecialAcc {
	private int id;
	private int specialAccNumber;
	private String metal;
	private String weightPure;
	private String weightAlloy;
	private String probe;
	private String gems;

	public SpecialAcc(int id, int specialAccNumber) {
		super();
		this.id = id;
		this.specialAccNumber = specialAccNumber;
	}
	public SpecialAcc(int id, int specialAccNumber, String gems) {
		super();
		this.id = id;
		this.specialAccNumber = specialAccNumber;
		this.gems = gems;
	}
	public SpecialAcc() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSpecialAccNumber() {
		return specialAccNumber;
	}
	public void setSpecialAccNumber(int specialAccNumber) {
		this.specialAccNumber = specialAccNumber;
	}
	public String getMetal() {
		return metal;
	}
	public void setMetal(String metal) {
		this.metal = metal;
	}
	public String getWeightPure() {
		return weightPure;
	}
	public void setWeightPure(String weightPure) {
		this.weightPure = weightPure;
	}
	public String getWeightAlloy() {
		return weightAlloy;
	}
	public void setWeightAlloy(String weightAlloy) {
		this.weightAlloy = weightAlloy;
	}
	public String getProbe() {
		return probe;
	}
	public void setProbe(String probe) {
		this.probe = probe;
	}
	public String getGems() {
		return gems;
	}
	public void setGems(String gems) {
		this.gems = gems;
	}
}
