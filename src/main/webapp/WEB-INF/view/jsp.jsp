<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="dao.*" import="java.util.*" import="model.*"  import="help.*"  import="java.io.*" %>
<%@include file = "TopThings.jsp" %>
<title>Fonds Database::Manage Users</title>
<%@include file = "SideBar.jsp" %>
				<h1>List of Users </h1>
				<table>
				<c:forEach var="user" items="${users}">
					<tr>
						<td> ${user.id}. 		</td>
						<td> ${user.name} ${user.surname}</td>
						<td> ${user.email}		</td>
						<td>
							<form action="deleteUser" method="POST">
								<input type="hidden" name = "id" value="${user.id}">
								<input type="submit" value="Delete" id="${user.id}">
							</form>
						</td>
						<td>
							<form action="editUser" method="POST">
								<input type="hidden" name = "id" value="${user.id}">
								<input type="hidden" name = "origin" value="<%out.print(Constants.USER_LIST); %>">
								<input type="submit" value="Edit" id="${user.id}">
							</form>
						</td>
					</tr>
				</c:forEach>
				</table>
			
				<form action="addUser" method="post">
					<table><tr>				
							<td valign="top">
		 						 <input  type="text" name="email" maxlength="50" size="30" placeholder="email">
							</td>
							<td valign="top">
		 						 <input  type="password" name="password" maxlength="50" size="30" placeholder="password">
							</td>
							<td>
								 <input type="submit" value="Add">   
							</td>
						</tr>				
					</table>
				</form>
<%@include file = "BottomThings.jsp" %>	